import React from "react";
import PropTypes from "prop-types";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPaperPlane } from '@fortawesome/free-solid-svg-icons';
import './index.css';

interface IMessageInputProps {
    addMessage: Function;
}

interface MessageInputState {
    messageBody: string;
  }

class MessageInput extends React.Component<IMessageInputProps, MessageInputState>{
    constructor(props: IMessageInputProps) {
        super(props);
        this.state = {
          messageBody: "",
        };
        this.typing = this.typing.bind(this);
      }

      static props = {
        addMessage: PropTypes.func,
      };

      typing(event: React.FormEvent<HTMLInputElement>) {
        this.setState({ messageBody: event.currentTarget.value });
      }

      render() {
          const data = this.state;
        return (
          <div className="message-input">
            <form className="message-input-text">
              <input
                type="text"
                className="text-area"
                placeholder="Message"
                value={data.messageBody}
                onChange={this.typing}
              />
            </form>
            <button className="message-input-button"
              onClick={() => this.props.addMessage(data.messageBody)}
            >
              <FontAwesomeIcon icon={faPaperPlane} className="send"/>
            </button>
          </div>
        );
      }
    }

export default MessageInput;