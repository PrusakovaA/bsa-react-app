import React from "react";
import Header from "../Header/index";
import MessageInput from "../MessageInput/index";
import Preloader from "../Preloader/index";
import MessageList from "../MessageList/index";
import Message from "../../IMessage";
import { Api } from "../../services/api";
import AppHeader from "../AppHeader";
import data from "../../services/DataService";
import "./index.css";


interface IChatProps{
  url: string
}
interface ChatState {
    isLoading: boolean;
    usersCount?: number;
    messagesCount?: number;
    messages?: Message[];
    lastMessageDate?: string;
    editedMessage?: Message;
    time?:string;
}

class Chat extends React.Component<IChatProps,ChatState> {
  api: Api;

    constructor(props: IChatProps) {
        super(props);
        this.api = new Api(props.url);  
        this.state = {
          isLoading: true,
          messages: []
        };
    this.addMessage = this.addMessage.bind(this);
    this.deleteMessage = this.deleteMessage.bind(this);
    this.editMessage = this.editMessage.bind(this);
    }

    componentDidMount() {
      this.api.loadData().then(({ messages, usersCount, messageCount }) => {
        this.setState({
          isLoading: false,
          messages: messages,
          usersCount: usersCount,
          messagesCount: messageCount,
          lastMessageDate: messages[messages.length - 1].time,
        });
      }); 
      }
      

    addMessage(text: string) {
        if (text) {
            const messages = this.state.messages;
            const date = new Date();
            messages!.push({
              id: date.getTime().toString(),
              text,
              userId: "6e2439ns-83a5-19e9-6e0c-8f1s686f4ce4",
              user: "Me",
              createdAt: date,
              time: data.getTime(date),
            });
            const count = this.state.messagesCount! + 1;
            const usersCount = this.api.getUsersCount(messages);
            this.setState({
              messages,
              messagesCount: count,
              lastMessageDate: data.getTime(date),
              usersCount,
              time: data.getTime(date),
            });
          }
    }

    deleteMessage(message: Message) {
    const messages = this.state.messages;
    for (let i = 0; i < messages!.length; i++) {
      if (messages![i].id === message.id) {
        messages?.splice(i, 1);
      }
    }
    const count = messages?.length;
    this.setState({ messages, messagesCount: count});
    }

    editMessage(message: Message, text: string) {
        let messages = this.state.messages!;
    for (let i = 0; i < messages!.length; i++) {
      if (messages![i].id === message.id) {
        messages![i].text = text;
      }
    }
    this.setState({ messages});
    }

    render() {
        const data = this.state
        return (<div className="chat">
        <AppHeader />
          {data.isLoading 
                ? (<Preloader />) : (
                  <div className="chat-container">
                <Header
                title={"My chat"}
                usersCount={data.usersCount!}
                messagesCount={data.messagesCount!}
                lastMessageDate={data.lastMessageDate!}
                />
                <div className="chat-wrapper">
                <MessageList
                messages={data.messages!}
                deleteMessage={this.deleteMessage}
                editMessage={this.editMessage}
                time={data.time!}
              />
                </div>
                <MessageInput addMessage={this.addMessage} />
            </div>
                )
    }
           </div>
           
        );
    }
}
export default Chat;    
