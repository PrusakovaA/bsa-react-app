import React from "react";
import PropTypes from 'prop-types';
import Message from "../../IMessage";
import message from "../../services/DataService";
import OwnMessage from "./OwnMessage";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHeart } from '@fortawesome/free-solid-svg-icons';
import "./index.css";
interface IMessageProps {
    message: Message;
    userId: string;
    deleteMessage: Function;
    editMessage: Function;
    time: string;
}

interface MessageState {
    likesMessage: number;
}

class MessageClass extends React.Component<IMessageProps, MessageState> {
    static props = {
        message: PropTypes.object,
        editMessage: PropTypes.func,
        deleteMessage: PropTypes.func,
        time: PropTypes.string
      };

      constructor(props: IMessageProps) {
        super(props);
        this.state = {
            likesMessage: 0,
        };
        this.addLike = this.addLike.bind(this);
      }

      addLike() {
        let likes = this.state.likesMessage;
        this.setState({ likesMessage: likes ? 0 : 1 });
      }
      render() {
        return <div className="message">
        {(message.isOwnMessage(this.props.message) ?(
        <OwnMessage
            message={this.props.message}
            deleteMessage={this.props.deleteMessage}
            editMessage={this.props.editMessage}
            time={this.props.time}
        />):
        (<div className="others-message">
        <div className="message-content">
        <div className="message-user-avatar">
            <img
              className="avatar"
              src={this.props.message.avatar}
              alt="avatar"
            />
          </div>
          <div className="text">
        <div className="message-text">{this.props.message.text}</div>
        </div>
          <div className="message-info">
          <span className="message-user-name">{this.props.message.user}</span>
            <span className="message-time">{this.props.message.time}</span>
            <div className="like" onClick={() => this.addLike()}>
              <span>{this.state.likesMessage ? <FontAwesomeIcon icon={ faHeart} className="message-liked"/> 
              : <FontAwesomeIcon icon={ faHeart} className="message-like"/>}</span>
          </div>
          </div>
      </div>  
      </div>
        )
        )}
        </div>
      }
}

export default MessageClass;