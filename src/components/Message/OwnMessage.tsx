import React from "react";
import Message from "../../IMessage";
import PropTypes from "prop-types";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEdit } from '@fortawesome/free-solid-svg-icons';
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import "./index.css";

interface IOwnMessageProps {
  message: Message;
  deleteMessage: Function;
  editMessage: Function;
  time: string;
}

interface OwnMessageState{}

class OwnMessage extends React.Component<IOwnMessageProps, OwnMessageState>{

    static props = {
        message: PropTypes.object,
        deleteMessage: PropTypes.func,
        editMessage: PropTypes.func,
        time: PropTypes.string
      };

   

      render() {
        return (
        <div className="own-message">
                <div className="message-text">
                    {this.props.message.text}
                    </div>
            <div className="message-info">
                <span className="me-user"></span>
                <span className="message-time">{this.props.time}</span>
                <div
                        className="message-edit"
                        onClick={() => this.props.editMessage(this.props.message)}
                >
                         <FontAwesomeIcon icon={faEdit} />
                    </div>
                    <div
                        className="message-delete"
                        onClick={() => this.props.deleteMessage(this.props.message)}
                    >
                         <FontAwesomeIcon icon={faTrashAlt} />
                    </div>
                </div>
        </div>
        );
      }
}   

export default OwnMessage;