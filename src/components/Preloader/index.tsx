import React from "react";
import "./index.css";

export default class Preloader extends React.Component {
  render() {
    return (
      <div className="preloader"></div>
    );
  }
}