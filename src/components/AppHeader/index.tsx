import React from 'react';

import './index.css';

const AppHeader = () => (
  <header className="header">
    <p>Chats</p>
  </header>
);

export default AppHeader;