import React from "react";
import PropTypes from "prop-types";
import "./index.css";
import Message from "../../IMessage";
import MessageClass from "../Message";
import api from "../../services/DataService";

interface IMessageListProps {
    messages: Message[];
    editMessage: Function;
    deleteMessage: Function;
    time: string;
}

class MessageList extends React.Component<IMessageListProps>{
    static props = {
        messages: PropTypes.array,
        editMessage: PropTypes.func,
        deleteMessage: PropTypes.func,
        time: PropTypes.string
      };

      render() {
        return (
          <div className="message-list">
             {api.groupByDate(this.props.messages).map((groupsByDate, id) => (
          <div className="message-list-group" key={id}>
            <div  key={groupsByDate.date} className="messages-divider">{groupsByDate.date}</div>
            {groupsByDate.messages.map((message: Message, id: string) => (
              <MessageClass
                userId={id}
                message={message}
                editMessage={this.props.editMessage}
                deleteMessage={this.props.deleteMessage}
                time={this.props.time}
              />
            ))}
          </div>
        ))}
          </div>
        );
        
      }
}

export default MessageList;