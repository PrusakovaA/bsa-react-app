import React from "react";
import PropTypes from "prop-types";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faComments } from '@fortawesome/free-solid-svg-icons';
import { faUsers } from '@fortawesome/free-solid-svg-icons';
import {faEnvelopeOpen } from '@fortawesome/free-solid-svg-icons';
import "./index.css";
interface IHeaderProps {
    title: string;
    usersCount: number;
    messagesCount: number;
    lastMessageDate: string;
}

interface HeaderState {}
class Header extends React.Component<IHeaderProps, HeaderState> {
    static props = {
    title: PropTypes.string,
    usersCount: PropTypes.number,
    messagesCount: PropTypes.number,
    lastMessageDate: PropTypes.string,
    };
    render() {
        return (
            <div className="chat-header">
            <div className="header-title">{this.props.title}
            <FontAwesomeIcon icon={faComments} className="chatIcon"/>
            </div>
            <div className="chat-info">
                <span className="header-users-count">{this.props.usersCount} participants <FontAwesomeIcon icon={faUsers} /></span>
                <span className="header-messages-count">{this.props.messagesCount} messages <FontAwesomeIcon icon={faEnvelopeOpen} /></span>
            </div>
            <div className="last-message">
                    <span className="header-last-message-date">last message at: {this.props.lastMessageDate}</span>
                </div>
        </div>
        );
    }
}

export default Header;