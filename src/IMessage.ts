export default interface Message {
    id: string;
    userId: string;
    avatar?: string;
    user: string;
    text: string;
    editedAt?: Date | string;
    createdAt: Date | string;
    likesCount?: number;
    time?: string;
  }