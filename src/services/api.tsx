import Message from "../IMessage";
import DataService from "./DataService";

export class Api {
  url: string = '';
  constructor(url: string) {
    this.url = url
}

    async getMessages() {
        const url: string = this.url;
        try {
            let res = await fetch(url);
          return await res.json();
        } catch (error) {
            console.log(error);
        }
    }

    async loadData() {
      this.getMessages();
      const messages = await this.getMessages();
      messages.map((messages: { time: string; createdAt: string | Date; }) => (
        messages.time = DataService.getTime(messages.createdAt)
        ));
      const usersCount = this.getUsersCount(messages);
      const messageCount = messages.length;
      return { messages, usersCount , messageCount };
    }

    getUsersCount(messages: Message[] | undefined) {
      if(messages === undefined) return 0;
      const list = new Set();
      messages.map((message) => (
        list.add(message.user)
      ));
      return list.size;
    }
    
}



